# Frkl Private License

This is a fork of the [licensezero private license](https://github.com/licensezero/licensezero-private-license), with sub-licensing removed. Check out that repository for its history, and how to read it.
